fun main(args: Array<String>) {
    val cadena = 3
    esCadenaa(cadena)
    obtaman(cadena)

}

//identificar si un ibjeto pertenece a una clase o no
fun esCadenaa(obj: Any) {

    if (obj is String) {
        println("Es una cadena")
    } else {
        println("No es una cadena")
    }

}

fun obtaman(obj:Any){
    //casteo  segura nos devuelve un nullable si no es el tipo de dato
    var cadena:String? = obj as? String
    println(cadena?.length)
}