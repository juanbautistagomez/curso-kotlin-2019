fun main(args: Array<String>) {

    //Se reprecentan con la clase Array acepta un tipo de dato generico puede ser numeros enteros, flotantes , string  o mesclas

    var arregloMezclado = arrayOf(1, 2, 3, 4, 5, 6, 7, "dd", 1.4f)

    // arreglo int
    var arregloInt = intArrayOf(1, 2, 3, 4, 5)

    // arreglo Float
    var arregloFloat = floatArrayOf(1.3f, 3f, 4f)

    // arreglo Null si no sabes que tipo sera tu arreglo
    var arregloNull = arrayOfNulls<String>(10)

    // Recorrer arreglo con ciclo for

    for (elem in arregloMezclado) {
        println(elem)
    }

    //ACCEDER EL INDICE Y POSTERIOR OBTENER EL ELEMENTO

    for (ind in arregloMezclado.indices) {
        println(arregloMezclado[ind])

        //el valor de indice
        println(ind)
    }

}