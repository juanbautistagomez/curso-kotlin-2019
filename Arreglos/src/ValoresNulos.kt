fun main(args: Array<String>) {

    // se de agrega ? para permitir valor nulo de lo contrario marcaria error pointexeption
    var tacos: String? = "Tacos"

    //tacos = null


    val tacosDePastor = tacos?.length

    println("Tengo $tacosDePastor de al pastor")

    //operador de llamada segura !! no permite valores nulos
    val tacosDetripa:Int = tacos!!.length

    println("Tengo $tacosDetripa de al tripa")

}