fun main(args: Array<String>) {
    esCadena("Este es una cadena")
}

//identificar si un ibjeto pertenece a una clase o no
fun esCadena(obj: Any) {

    if (obj is String) {
        println("Es una cadena")
    } else {
        println("No es una cadena")
    }

}