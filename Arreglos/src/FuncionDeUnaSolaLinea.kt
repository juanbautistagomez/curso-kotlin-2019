fun main(args: Array<String>) {

    val n1 = 2
    val n2 = 7
    val result = multi(n1, n2)

    //$ = interpolacion
    println("La multiplicaion de $n1 * $n2 = $result")

}

//Se crea la funcion multi

fun multip(n1: Int, n2: Int)= n1*n2