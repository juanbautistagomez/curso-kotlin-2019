fun main(args: Array<String>) {

    val nums = intArrayOf(1, 2, 3, 4, 5, 6)
    nums.ind(0, 4)

    println(nums[4])
}

fun IntArray.ind(ind1: Int, ind2: Int) {

    val temp = this[ind1]
    this[ind1] = this[ind2]
    this[ind2] = temp
}