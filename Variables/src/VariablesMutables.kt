fun main(args: Array<String>){

    //variables inmutables
    val x: Int
    x=3

    //se puede simplificar la varible inmutable
    val y = 3

    val result = x*y

    //variable mutable
    var xx:Int
    xx=3

    //variable mutable
    var yy = 3

   var resultado= yy * xx

    print(" variable inmutable multi = "+result)
    print(" variable mutable multi = "+resultado)


     xx= 4

    yy= 4

    resultado = 4 +4
    print(" variable mutable cambiando de valor: multi = "+resultado)

}